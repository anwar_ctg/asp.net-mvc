﻿mainApp.controller('itemsController', function ($scope, $http) {
    $http({ method: 'GET', url: 'api/values' })
        .success(function (data, status) {
            $scope.items = data;
            $scope.status = status;
        })
        .error(function (data, status) {

        });

    $scope.bookmarks = [];

    function AddToBookmarks(id)
    {
        $scope.bookmarks.push(id);
    }

    function RemoveFromBookmarks(id)
    {
        var index = $scope.bookmarks.indexOf(id);
        $scope.bookmarks.splice(index, 1);
    }

    $scope.AddDeleteBookmark = function(id)
    {
        if ($scope.bookmarks.indexOf(id) == -1)
            AddToBookmarks(id);
        else
            RemoveFromBookmarks(id);
    }

    $scope.IsBookmarked = function (id) {
        if ($scope.bookmarks.indexOf(id) == -1)
            return false;

        return true;
    }

    // Sort all items by price
    // Default sort by cost is from lower to higher
    $scope.sortedByCost = false;
    // Default sort by existing is from exist to not exist
    $scope.sortedByExisting = false;
    // 0 - Default type (without sort)
    // 1 - Compare by cost
    // 2 - Compare by exist
    $scope.sortType = 0;

    function compareElementsByCost(a, b) {
        if ($scope.sortedByCost) {
            var temp = a;
            a = b;
            b = temp;
        }

        return a.Currency.price - b.Currency.price;
    }

    $scope.doSortByCost = function () {
        $scope.items.sort(compareElementsByCost);

        $scope.sortedByCost = !$scope.sortedByCost;
        $scope.sortType = 1;
    }

    // Sort all items by existing at store
    function compareElementsByExisting(a, b) {
        if ($scope.sortedByExisting) {
            var temp = a;
            a = b;
            b = temp;
        }

        if (a.available == true && b.available == false)
            return 1;
        if (a.available == false && b.available == true)
            return -1;
        return 0;
    } 

    $scope.doSortByExisting = function () {
        $scope.items.sort(compareElementsByExisting);

        $scope.sortedByExisting = !$scope.sortedByExisting;
        $scope.sortType = 2;
    }

    // Search items in database
    $scope.findItems = function ()
    {
        var data = { searchQuery: $scope.searchQuery, registerCaseImportant: $scope.registerCaseImportant };

        $http.post('api/values', data)
        .success(function (data, status) {
            $scope.items = data;
            $scope.status = status;
        })
        .error(function (data, status, response) {
            window.alert('Status: ' + status);
        });
    }
});