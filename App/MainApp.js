﻿var mainApp = angular.module('mainApp', ['ngRoute', 'angular-loading-bar', 'ngCookies', 'ngAnimate']);

mainApp.config(function ($routeProvider, USER_ROLES) {
    // Route main page
    $routeProvider.when("/", {
        controller: "mainPageController",
        templateUrl: "App/Views/main.html",
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    // Route items page
    $routeProvider.when("/items", {
        controller: "itemsController",
        templateUrl: "App/Views/items.html",
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    // Route categories page
    $routeProvider.when("/categories", {
        controller: "categoriesController",
        templateUrl: "App/Views/categories.html",
        data: {
            authorizedRoles: [USER_ROLES.all]
        }
    });

    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "App/Views/login.html",
        data: {
            authorizedRoles: [ USER_ROLES.guest ]
        }
    });

    $routeProvider.when("/register", {
        controller: "registerController",
        templateUrl: "App/Views/register.html",
        data: {
            authorizedRoles: [USER_ROLES.guest]
        }
    });

    // Other cases
    $routeProvider.otherwise({ redirectTo: "/" });
});

mainApp.run(['$rootScope', '$cookieStore', '$location', 'AuthenticationService', 'AUTH_EVENTS', function ($rootScope, $cookieStore, $location, AuthenticationService, AUTH_EVENTS) {
    $rootScope.AuthenticationService = AuthenticationService;

    $rootScope.$on('$routeChangeStart', function (event, next) {
        var nextPageAuthorizedRoles = next.data.authorizedRoles;
        var resultAuthorized = AuthenticationService.isAuthorized(nextPageAuthorizedRoles);
        if (!AuthenticationService.isAuthorized(nextPageAuthorizedRoles)) {
            event.preventDefault();
            var resultAuthenticated = AuthenticationService.isAuthenticated();
            if (AuthenticationService.isAuthenticated()) {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
            }
            else {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
            }
        }

    });

    function getCookiesData() {
        var storedCookieData = $cookieStore.get('AuthData');
        if (undefined === storedCookieData)
            return { login: '', pass: '' };

        var cookieData = storedCookieData.replace('\"', '').split(':');
        var credentials = {
            login: cookieData[0],
            pass: cookieData[1]
        };

        return credentials;
    }

    // Calling a method for authentication before application start
    $rootScope.AuthenticationService.login(getCookiesData());
}]);