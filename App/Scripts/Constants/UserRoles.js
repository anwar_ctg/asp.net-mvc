﻿// Defining user roles for using in application' authorization
// and authentication services

mainApp.constant('USER_ROLES', {
    admin: 'admin',
    editor: 'editor',
    guest: 'guest',
    all: '*'
});