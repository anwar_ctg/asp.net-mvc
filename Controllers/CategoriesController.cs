﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using EShop.Entities;

namespace EShop.Controllers
{
    public class CategoriesController : ApiController
    {
        OzonDatabaseContext context = new OzonDatabaseContext();

        // GET api/categories
        public IEnumerable<Category> Get()
        {
            // Delete data about offer for minimize size of JSON-response
            List<Category> categories = context.Categories.ToList();

            return categories;
        }

        // GET api/categories/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/categories
        public void Post([FromBody]string value)
        {
        }

        // PUT api/categories/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/categories/5
        public void Delete(int id)
        {
        }
    }
}